## ENLACES TALLER THE WALL CODEMOTION 2020

- ETHEREUM DEVELOPERS:
https://ethereum.org/en/developers/

- ENTORNOS DE APRENDIZAJE:
https://ethereum.org/en/developers/learning-tools/
	- REMIX:
https://remix.ethereum.org/


- LOCAL STACK:
https://ethereum.org/en/developers/local-environment/

- TRUFFLE:
https://www.trufflesuite.com
	- TRUFFLE:
https://www.trufflesuite.com/truffle
	- GANACHE:
https://www.trufflesuite.com/ganache


- BALANCER CORE:
https://github.com/balancer-labs/balancer-core


- THE WALL:
https://github.com/jesusperezsanchez/thewall
	- CONTRATO THE WALL:
https://github.com/jesusperezsanchez/thewall/blob/master/contracts/Thewall.sol


- DIRECTORIO DE TRABAJO:
https://gitlab.com/juanantoniolleo/the_ground

- EJEMPLO DE USO DE ORACULO:
https://gitlab.com/juanantoniolleo/the_ground/-/blob/master/The_Wall/Contracts/The_Wall-Oraculo.sol

- CHAINLINK - CONSULTA AL ORACULO:
	- UN SOLO DATO:
https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD
	- CONSULTA COMPLETA:
https://min-api.cryptocompare.com/data/pricemultifull?fsyms=ETH&tsyms=USD