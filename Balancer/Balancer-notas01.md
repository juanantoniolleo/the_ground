Balancer:
Balancer-notas01.txt

https://balancer.finance/

- BALANCER:
Balancer is a non-custodial portfolio manager, liquidity provider, and price sensor

- DOCS:
Balancer is an automated portfolio manager, liquidity provider, and price sensor.
Balancer turns the concept of an index fund on its head: instead of a paying fees to portfolio managers to rebalance your portfolio, you collect fees from traders, who rebalance your portfolio by following arbitrage opportunities.
Balancer is based on an N-dimensional invariant surface which is a generalization of the constant product formula described by Vitalik Buterin and proven viable by the popular Uniswap dapp.

- SMART CONTRACTS:
https://docs.balancer.finance/smart-contracts/index

- API:
https://docs.balancer.finance/smart-contracts/api

- DEVELOPMENT:
https://docs.balancer.finance/sor/development

- GUIDES:
	- TESTING ON KOVAN:
https://docs.balancer.finance/guides/testing-on-kovan

	- MAKING A TRADE:
https://docs.balancer.finance/guides/making-a-trade
Making_a_Trade-sor_diagram.jpg

	- CREATING A BALANCER POOL:
https://docs.balancer.finance/guides/creating-a-balancer-pool
	CREATING AND CONFIGURING A BALANCER POOL VIA ETHERSCAN
If you’re all about beautiful UIs and smooth UXs, please wait until Balancer Labs comes up with the perfect dapp for creating your own pool with one click.
This tutorial is for the trailblazers. It purposefully goes over each transaction needed and by the time you’re done you should understand what actually happens under the hood. Maybe this even inspires you to create your own shiny toys on top of Balancer.
You’ll need to have Metamask installed (or an alternative Web3 wallet/browser) and have acquired a small amount of the tokens desired for your pool. Some basic knowledge of how Ethereum works is highly desirable, but not an absolute requirement. Let’s go!

https://pools.balancer.exchange/#/pool/0x75286e183D923a5F52F52be205e358c5C9101b09


- DOCUMENTACION:
https://docs.balancer.finance/

	- AUDITORIA:
Trail_of_Bits-Full_Audit.pdf


- GITHUB:
https://github.com/balancer-labs


- PASAR A PDF:
https://balancer.finance/whitepaper/

-



