## Sets-ETH50SMACO-notas01
Sets-ETH50SMACO-notas01.txt

PAGINA PRINCIPAL:
ETH 50 DAY MA CROSSOVER SET: ETH50SMACO
https://www.tokensets.com/set/eth50smaco
The ETH 50 Day Moving Average Crossover Set attempts to capitalize on medium term price trends and accumulate ETH. ETH50SMACO automatically triggers rebalances when the price of ETH crosses the 50 Day Simple Moving Average (50 SMA) indicating a trend reversal. If the price of Ethereum crosses and stays below the 50 SMA, ETH50SMACO rebalances ETH into cash (USDC) to reduce further losses in the downtrend. If the price of Ethereum crosses and stays above the 50 SMA, ETH50SMACO rebalances back into ETH to capture upside in the trend reversal. Link to more details. (Copiado más abajo)
https://help.tokensets.com/en/articles/3176106-eth-50-day-moving-average-crossover-details

	- PANTALLA:
Current Price
$214.52

24HR Price Change
$1.52

24HR Percent Change
0.71%

Market Cap
$202.2k
	- BOTONES:
	BUY
	SELL
- GRAFICO INTERACTIVO
Token | Quantity per Set | Token Price | Current Price Allocation | Target Price Allocation | Percent Change | Total Price per Set

ETH 50 Day MA Crossover Set 24.79% $214.52
UNDERLYING TOKENS:
Wrapped Eth
1.00757WETH
$212.91
100%
0%
26.02%
$214.52

USD
0USDC
$1.00
0%
100%
-0.09%
$0.00

NEXT REBALANCE:
All Sets rebalance when strategy criteria are met. For Trend Trading Sets, the criteria are time since last rebalance, and the indicator. Learn how rebalances work.
https://help.tokensets.com/en/articles/3153142-how-rebalances-work

Time Since Last Rebalance
16 days
4 days required

Plus sign
Price chart
Price status

Crosses 50 Day Average
$212.91
≤ $155.75

Equal sign
Balance scale
Rebalancing status

Is Rebalance Ready?
No

Key Facts
Rebalancing Phase
Normal
Last Rebalance
April 16th 2020
Rebalancing Interval
4 days
Rebalancing Tolerance
ETH Price ≤ $155.75
Inception Date
August 29th 2019
Set Address
Copy Address
0xa360F2aF3F957906468c0FD7526391AeD08aE3DB
Etherscan Link
View on Etherscan
https://www.etherscan.io/address/0xa360F2aF3F957906468c0FD7526391AeD08aE3DB

- Token ETH 50 SMA Crossover Set:
https://etherscan.io/token/0xa360f2af3f957906468c0fd7526391aed08ae3db

ETH 50 SMA Crossover Set (ETH50SMACO)

- Contract 0xa360F2aF3F957906468c0FD7526391AeD08aE3DB
https://etherscan.io/address/0xa360f2af3f957906468c0fd7526391aed08ae3db
	- CODIGO:
https://etherscan.io/address/0xa360f2af3f957906468c0fd7526391aed08ae3db#code

MANAGEMENT STRATEGY:
- Trend Trading
Trend Trading Sets aim to capitalize on the price trend of a target crypto asset using popular technical trading indicators such as moving averages. Trend Trading Sets automatically rebalances into a stable asset on signals confirming bearish trends and into the target asset on signals confirming bullish trends.

TALK TO A TEAM MEMBER

---------------------------------------------------------------
ARTICULO EXPLICATIVO:
TITULO:
ETH 50 DAY MOVING AVERAGE CROSSOVER DETAILS:
Learn about details under the hood for the ETH50SMACO Set 
https://www.tokensets.com/set/eth50smaco

https://help.tokensets.com/en/articles/3176106-eth-50-day-moving-average-crossover-details

https://www.tokensets.com/set/eth20smaco

- HOW DOES THE ETH 50 DAY MOVING AVERAGE CROSSOVER SET WORK?
The ETH 50 Day Moving Average Crossover Set
https://www.tokensets.com/set/eth50smaco
implements a crossover strategy on the ETH 50 day simple moving average indicator.

Learn more about moving averages and crossovers here. 
https://help.tokensets.com/en/articles/2941377-what-are-moving-averages

On a high level, ETH50SMACO automatically triggers rebalances when the price of ETH crosses the 50 Day Simple Moving Average (50 SMA) indicating a trend reversal. See image below.
ETH50SMACO-Set-explanation.png

- HOW IS THE ETH50SMACO DIFFERENT FROM ETH20SMACO?
In contrast to the ETH 20 Day Moving Average Crossover Set,
https://www.tokensets.com/set/eth20smaco
the ETH50SMACO rebalances based on the longer term trend. This means rebalances are less frequent with the goal of filtering out short term price spikes and dips. Both ETH20SMACO and ETH50SMACO can be used in combination to diversify exposure to both long term and short term trend trading for ETH.

- WHY USE THE 50 DAY SIMPLE MOVING AVERAGE?
The 50 Day Simple Moving Average (50 SMA) is one of the most popular technical indicators to determine when to enter and exit positions for medium to longer term traders. 

- HOW DOES THE SET PERFORM?
Disclaimer: The content below is for informational purposes only, you should not construe any such information or other material as legal, tax, investment, financial, or other advice. The content below is provided for educational purposes only, and not indicative of future performance. None of the following should be interpreted as investment advice. The tools used below follow a predefined set of parameters and aren’t actively managed by Set Labs Inc.

View a model of the backtest here.
https://docs.google.com/spreadsheets/d/1tD5OyaUdUsgmvOhCRmBvWJzCYsHmcremuX5cvdFgpGk/edit#gid=555100138
This chart below compares the hypothetical performance of holding the ETH50SMACO Set against holding ETH over the last 3 years (August 20, 2016 to August 20, 2019), assuming a slippage rate of 1% each rebalance. The model shows ETH50SMACO would have outperformed over the given historical time period and bear markets, and underperformed in choppy trading conditions and long bull markets (late 2017 - early 2018).
50DaySMACrossVsHolding100pcETH.png

The chart below illustrates the ETH that would have been accumulated using the ETH50SMACO Set following the same assumptions above.
ETH_Accumulated_using_50DaySMACrossover_Strategy.png

It’s important to keep in mind the the charts above are based on a number of assumptions and are only meant for illustrative purposes. As always, past performance is not indicative of future performance.

The model
https://docs.google.com/spreadsheets/d/1tD5OyaUdUsgmvOhCRmBvWJzCYsHmcremuX5cvdFgpGk/edit#gid=555100138
uses generated ETH hourly data sourced from Gemini. In production, the ETH50SMACO Set rebalances based on MakerDAO’s ETH price feed, which may output different results.  If you’d like to play around with the data yourself (and tweak the parameters), feel free to fork the spreadsheet here.
https://docs.google.com/spreadsheets/d/1tD5OyaUdUsgmvOhCRmBvWJzCYsHmcremuX5cvdFgpGk/edit#gid=555100138

- IN WHAT MARKETS DO THE ETH50SMACO TYPICALLY OUTPERFORM AND UNDERPERFORM?
ETH50SMACO typically outperforms in strong longer term directional markets (bear or bull), and typically underperforms in choppy markets as moving averages are lagging indicators. 
https://www.investopedia.com/terms/c/choppymarket.asp

- HOW IS THE 50 SMA CALCULATED ON TOKENSETS?
The 50 SMA indicator is calculated by averaging the on-chain MakerDAO ETH price
https://makerdao.com/feeds/
for the past 50 days at approximately 7:05 pm UTC each day. Link to technical documentation of the SMA oracle here.
https://github.com/SetProtocol/set-protocol-strategies/wiki

- WHAT ARE THE ADDITIONAL CRITERIA NEEDED TO TRIGGER A REBALANCE?
The rebalance criteria includes a minimum interval of 96 hours between rebalances and a confirmation period after the initial price-indicator crossover to reduce false positive signals. If the signal at the time of the initial crossover (e.g. price > moving average) matches the signal after 6 hours, then the Set will kickoff a rebalance. If the signal fails to match the signal after 12 hours, then the rebalance expires and another price-indicator crossover must occur.

- HOW LONG DOES THE REBALANCE TAKE TO COMPLETE?
Rebalances for the ETH50SMACO take approximately 2 hours to complete. Price drift is on average <1% over the course of 2 hours based on historical MakerDAO ETH price data. Learn more about Set rebalances here.
https://help.tokensets.com/articles/2862890-what-is-the-rebalancing-phase

View the ETH 50 Day Moving Average Crossover Set.
https://www.tokensets.com/set/eth50smaco



GRAFICOS:
50DaySMACrossVsHolding100pcETH.png