Chainlink-Notas01.txt

https://chain.link/

## WHAT IS CHAINLINK?:
https://docs.chain.link/docs/what-is-chainlink
Chainlink is a general-purpose framework for building decentralized oracle networks that give your smart contract access to secure and reliable data inputs and outputs.
Each decentralized oracle network consists of a collection of independent node operators, a method for aggregating data, and pre-made "Chainlinks" (also called external adaptor) that act as middleware to give you access to any API you want to leverage for data and/or external services.
You can take advantage of existing oracle networks, such as our Price Reference Data feeds:
https://feeds.chain.link/
for DeFi, providing highly accurate market prices, or you can build your own oracle network and external adaptor.
Chainlink currently provides decentralization at both the oracle and data source level. By using multiple independent Chainlink nodes, the user can protect against one oracle being a single point of failure. Similarly, using multiple data sources for sourcing market prices, the user can protect against one data source being a single source of truth. Both of these ensure that the oracle mechanism triggering your important smart contract is as secure and reliable as the underlying blockchain.
You can use Chainlink to connect to data providers, web APIs, enterprise systems, cloud providers, IoT devices, payment systems, other blockchains and much more.

### CONTRACT CREATORS - OVERVIEW:
Contract creators are developers who build smart contracts with the goal of connecting them to off-chain data sources.
####  GETTING STARTED
You can start creating your contracts now! Start with Create a Chainlinked Project:
https://docs.chain.link/docs/create-a-chainlinked-project
or the Example Walkthrough:
https://docs.chain.link/docs/example-walkthrough
We strongly recommend you deploy first to testnets before deploying to Ethereum mainnet. Chainlink provides a practical developer environment on Ropsten, Rinkey and Kovan.
You will also want to consider what kind of data and what data source you want to use.

#### ETH-USD:
https://feeds.chain.link/eth-usd


#### PRICE REFERENCE DATA:
Decentralized Oracle Networks for Price Reference Data
The Chainlink Network provides the largest collection of secure and decentralized on-chain price reference data available. Composed of security reviewed, sybil resistant and fully independent nodes which are run by leading blockchain devops and security teams. Creating a shared global resource which is sponsored by a growing list of top DeFi Dapps.
Please feel free to look into the details of each Decentralized Oracle Network listed below. You can easily use these oracle networks to quickly and securely launch, add more capabilities to and/or just greatly improve the security of your smart contracts.

Decentralized Price Reference Data for Fiat Pairs:
ETH / USD aggregation $ 184.83

#### GITHUB:
https://github.com/smartcontractkit/chainlink

#### WHITE PAPER:
https://link.smartcontract.com/whitepaper
ChainLink-WhitePaper.pdf

### DOCS:
https://docs.chain.link/docs

#### Decentralized Oracles (Testnet):
https://docs.chain.link/docs/testnet-oracles

#### USING MULTIPLE ORACLES TO PROVIDE GREATER RELIABILITY:
In addition to sending requests to individual oracles, you are also able to use multiple oracles that verify the accuracy of the same result. Using multiple oracles helps gurantee that the results provided to your smart contract are accurate, creating a high degree of assurance that your smart contract is being triggered correctly...

#### ROPSTEN:
Ropsten LINK address: 0x20fE562d797A42Dcb3399062AE9546cd06f63280
https://ropsten.etherscan.io/address/0x20fE562d797A42Dcb3399062AE9546cd06f63280
##### CONTRATO:
https://ropsten.etherscan.io/address/0x20fe562d797a42dcb3399062ae9546cd06f63280

#### CONFIGURACION:
https://docs.chain.link/docs/binance-chainlink-testnet#config

## EJEMPLO:
### BINANCE CHAINLINK (TESTNET):
https://docs.chain.link/docs/binance-chainlink-testnet
This Chainlink has a dedicated connection to Binance's API. This Chainlink will allow requesters to create queries to the API, and return the response.


### Chainlink Examples

The example below shows how to create requests for the adapter:

----------------------------------------------------------------------------
function getAvgPrice(address _oracle, bytes32 _jobId, string _symbol)
  public
  onlyOwner
{
  Chainlink.Request memory req = buildChainlinkRequest(_jobId, address(this), this.fulfillPrice.selector);
  req.add("endpoint", "avgPrice");
  req.add("symbol", _symbol);
  req.add("copyPath", "price");
  req.addInt("times", 100);
  sendChainlinkRequestTo(_oracle, req, oraclePayment);
}
----------------------------------------------------------------------------
