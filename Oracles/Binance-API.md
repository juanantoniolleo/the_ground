Ejemplo de lectura de datos desde el sitio de Binance:
- 
### BINANCE:

https://api.binance.com

https://binance-docs.github.io/apidocs/spot/en/#current-average-price
##### ESTO FUNCIONA:

https://api.binance.com/api/v1/ticker/24hr
##### GENERA UN JSON CON 7 PARES DE VALORES
##### E INCLUYE TODOS LOS PARES ¡¡¡800 EN TOTAL!!!
##### EL 0 ES ETHBTC: (COPIO EL CONTENIDO JSON)

##### ESTO FUNCIONA:
##### ETHBTC
https://api.binance.com/api/v1/ticker/24hr?symbol=ETHBTC

##### ESTO FUNCIONA: es el par LTC-BTC
https://api.binance.com/api/v1/ticker/24hr?symbol=LTCBTC
- DAN BASTANTES VALORES, POR EJEMPLO:
lastPrice	"0.00481100"

##### ESTO FUNCIONA: es el precio medio del par ETHBTC
https://api.binance.com/api/v3/avgPrice?symbol=ETHBTC


[{"symbol":"ETHBTC","priceChange":"-0.00083200","priceChangePercent":"-3.628","weightedAvgPrice":"0.02269712","prevClosePrice":"0.02293700","lastPrice":"0.02210000","lastQty":"0.00600000","bidPrice":"0.02209600","bidQty":"0.00900000","askPrice":"0.02210000","askQty":"0.00300000","openPrice":"0.02293200","highPrice":"0.02309600","lowPrice":"0.02205000","volume":"212161.37200000","quoteVolume":"4815.45145462","openTime":1588709272846,"closeTime":1588795672846,"firstId":174468126,"lastId":174571623,"count":103498},{"symbol":"LTCBTC","...

##### ETHEUR
{"symbol":"ETHEUR","priceChange":"-0.44000000","priceChangePercent":"-0.232","weightedAvgPrice":"192.61895277","prevClosePrice":"189.79000000","lastPrice":"189.56000000","lastQty":"0.72276000","bidPrice":"189.56000000","bidQty":"0.80731000","askPrice":"190.44000000","askQty":"0.46540000","openPrice":"190.00000000","highPrice":"195.39000000","lowPrice":"187.83000000","volume":"515.65817000","quoteVolume":"99325.53669380","openTime":1588709167932,"closeTime":1588795567932,"firstId":101893,"lastId":102771,"count":879},...

##### Pendiente: conseguir acceder sólo a uno de los datos
- Por ejemplo: Esto no funciona

https://api.binance.com/api/v1/ticker/24hr/ETHBTC
