Uniswap-desarrollo-notas01.txt


https://uniswap.org/docs/v2

### PRICING:
In Uniswap V1, trades are always executed at the “best possible” price, calcuated at execution time. Somewhat confusingly, this calculation is actually accomplished with one of two different formulas, depending on whether the trade specifies an exact input or output amount. Functionally, the difference between these two functions is fairly miniscule, but the very existence of a difference increases conceptual complexity. Initial attempts to support both functions in V2 proved inelegant, and the decision was made to not provide any pricing functions in the core. Instead, pairs directly check whether the invariant was satisfied (accounting for fees) after every trade. This means that rather than relying on a pricing fuction to also enforce the invariant, V2 pairs simply and transparently ensure their own safety, a nice separation of concerns. One downstream benefit is that V2 pairs will more naturally support other flavors of trades which may emerge, (e.g. trading to a specific price at execution time).

A similar pattern exists for adding liquidity. V2 pairs do not return tokens added at an incorrect price. As an example, if the ratio of x:y in a pair is 10:2 (i.e. the price is 5), and someone naively adds liquidity at 5:2 (a price of 2.5), the contract will simply accept all tokens (changing the price to 3.75 and opening up the market to arbitrage), but only issue pool tokens entitling the sender to the amount of assets sent at the proper ratio, in this case 5:1. To avoid donating to arbitrageurs, it is imperative to add liquidity at the current price. So, in Uniswap V2, trades and liquidity provisions must be priced in the periphery. The good news is that the library provides a variety of functions designed to make this quite simple, and the router does this by default.

### ORACLES:
Every pair in Uniswap V2 keeps track of two price accumulators. These values can power nearly any type of price oracle measuring the relative price of the pair assets. For an in-depth discussion of Uniswap V2 oracles, see the whitepaper.

https://uniswap.org/docs/v2/guides/oracles/

##### EXAMPLES:
ExampleOracleSimple.sol

##### MIS COMENTARIOS:
ExampleOracleSimple-Comentado01.sol

ExampleSlidingWindowOracle.sol

#### LIBRERIA:
Esta es la librería que hace el cálculo de los precios medios

https://github.com/Uniswap/uniswap-v2-periphery/blob/master/contracts/libraries/UniswapV2OracleLibrary.sol

##### IMPORTA LAS LIBRERIAS
IUniswapV2Pair.sol
FixedPoint.sol

##### LA PRIMERA ES LA DE LOS PARES, QUE GUARDA LOS PRECIOS ACUMULADOS
ESTA AQUÍ:

https://github.com/Uniswap/uniswap-v2-periphery/blob/master/contracts/libraries/UniswapV2Library.sol

##### ENLAZA CON ESTA OTRA:
IUniswapV2Pair.sol

##### QUE ESTA AQUÍ:
https://github.com/Uniswap/uniswap-v2-core/tree/master/contracts

https://github.com/Uniswap/uniswap-v2-core/blob/master/contracts/UniswapV2Pair.sol

¡¡¡ATENCION, ESTA EN UNA CARPETA DISTINTA: CORE, EN VEZ DE PERIPHERY!!!

## uniswap-v2-core

#### ROUTE:
https://uniswap.org/docs/v2/SDK/route/
##### MIDPRICE:
midPrice: Price
Returns the current mid price along the route.
##### EXECUTIONPRICE:
executionPrice: Price
The average price that the trade would execute at.
##### NEXTMIDPRICE:
nextMidPrice: Price
What the new mid price would be if the trade were to execute.

#### FRACTIONS:
https://uniswap.org/docs/v2/SDK/fractions/
##### PRICE:
Responsible for denominating the relative price between two tokens. Denominator and numerator must be unadjusted for decimals.
##### Example:
----------------------------------------------------
import { ChainId, WETH as WETHs, Token, Price } from '@uniswap/sdk'

const WETH = WETHs[ChainId.MAINNET]
const ABC = new Token(ChainId.MAINNET, '0xabc0000000000000000000000000000000000000', 18, 'ABC')

const price = new Price(WETH, ABC, '1000000000000000000', '123000000000000000000')
console.log(price.toSignificant(3)) // 123
----------------------------------------------------
This example shows the ETH/XYZ price, where ETH is the base token, and XYZ is the quote token. The price is constructed from an amount of XYZ (the numerator) / an amount of WETH (the denominator).

### CODIGO HTML DE LA WEB
##### ADD LIQUIDITY:
uniswap_exchange-add_liquidity.html



### ANALISIS UNISWAP:
##### Uniswap Exchange Template:
https://etherscan.io/address/0x2157a7894439191e520825fe9399ab8655e0f708

##### UNISWAP FACTORY CONTRACT:
##### Parece que es la principal, puedes ver movimientos pendientes
https://etherscan.io/address/0xc0a47dfe034b400b47bdad5fecda2621de6c4d95
##### CONTRATO: Contract Source Code (Vyper language format)
https://etherscan.io/address/0xc0a47dfe034b400b47bdad5fecda2621de6c4d95#code
##### pasado a: Vyper_contract.VYP
##### No sé cual es la extension para el lenguaje vyper

### PARES UNISWAP:
- Uniswap: DAI
https://etherscan.io/address/0x2a1530c4c41db0b0b2bb646cb5eb1a67b7158667