creating-a-balancer-pool-notas01.txt

### CREATING AND CONFIGURING A BALANCER POOL VIA ETHERSCAN:
https://docs.balancer.finance/guides/creating-a-balancer-pool


- POOL CREATION:
Balancer Pools are created by calling a function at the BFactory contract. So to get started we go to the “Write Contract” tab of the BFactory contract on Etherscan to check out the available functions.
https://etherscan.io/address/0x9424b1412450d0f8fc2255faf6046b98213b76bd#writeContract
	- GRAFICO:
assets_balancer_-Write_Contract.png
	- SMART CONTRACT:

	- EN ETHERSCAN:
https://etherscan.io/address/0x75286e183d923a5f52f52be205e358c5c9101b09#code


	- ANALITICA:
	- EN LA OPCION TOKEN TRANSFER:
	Parece que se mueve entre un rango superior e inferior
	Entre 100 y 135 ¿precio de ETH en $? REPASAR
https://etherscan.io/address/0xe5d1fab0c5596ef846dcc0958d6d0b20e1ec4498#analytics

	- TOKEN TRACKER:
	Es otro contrato inteligente
https://etherscan.io/token/0x75286e183d923a5f52f52be205e358c5c9101b09
	
	CONTRACT NAME: BPool
	Note: This contract matches the deployed ByteCode of the Source Code for Contract:
https://etherscan.io/address/0xe5d1fab0c5596ef846dcc0958d6d0b20e1ec4498#code
	- DIRECCION DEL CONTRATO:
https://etherscan.io/address/0x75286e183d923a5f52f52be205e358c5c9101b09#code

	- EVM bytecode decompiler (Experimental):
https://etherscan.io/bytecode-decompiler?a=0xe5d1fab0c5596ef846dcc0958d6d0b20e1ec4498
	ByteCode Decompilation Result:
	- EL RESULTADO ESTA EN EL ARCHIVO:
	¿no sé si es Solidity?
BPool-decompiled01.sol