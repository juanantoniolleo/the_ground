Oraculos-Solidity-Notas01.txt

## SOLIDITY-PATTERNS:
A compilation of patterns and best practices for the smart contract programming language Solidity

#### ORACLE:
https://fravoll.github.io/solidity-patterns/oracle.html
#### CODIGO EN SOLIDITY:
https://github.com/fravoll/solidity-patterns/tree/master/Oracle

ATENCION, USA UNA LIBRERIA QUE NO ESTÁ DONDE APUNTA:

github.com/oraclize/ethereum-api/oraclizeAPI_0.4.sol

PERO HE ENCONTRADO EL ENLACE CORRECTO. HABRIA QUE MODIFICAR EL CODIGO:

https://github.com/provable-things/ethereum-api/blob/master/oraclizeAPI_0.4.sol

LO MISMO SE PUEDE PARTIR DE ESTE EJEMPLO Y ADAPTARLO PARA QUE LEA DESDE UN SITIO QUE COMPARE CRYPTO Y FIAT.

-----------------------------------------------------------------------------
// This code has not been professionally audited, therefore I cannot make any promises about
// safety or correctness. Use at own risk.

pragma solidity ^0.4.20;
import "github.com/oraclize/ethereum-api/oraclizeAPI_0.4.sol";

contract Oracle is usingOraclize {

    string public EURUSD;

    function updatePrice() public payable {
        if (oraclize_getPrice("URL") > this.balance) {
            //Handle out of funds error
        } else {
            oraclize_query("URL", "json(https://api.exchangeratesapi.io/latest?symbols=USD).rates.USD");
        }
    }

    function __callback(bytes32 myid, string result) public {
        require(msg.sender == oraclize_cbAddress());
        EURUSD = result;
    }
}
-----------------------------------------------------------------------------



https://github.com/fravoll/solidity-patterns


#### Ethereum Oracle Contracts: Solidity Code Features
https://hackernoon.com/ethereum-oracle-contracts-solidity-code-features-15d767776e44

-----------------------------------------------------------------------------

### CRYPTONATOR: CRYPTOCURRENCIES EXCHANGE RATES API:
Cryptonator provides unique volume of cryptocurrency exchange rates data, which is delivered in easy-to-integrate JSON format via simple HTTPS requests.
Prices are updated every 30 seconds, covering 300+ cryptocurrencies across 67 exchanges.
https://www.cryptonator.com/api/

-----------------------------------------------------------------------------
##### SIMPLE TICKER: Returns actual volume-weighted price, total 24h volume and the price change.
Example request for BTC-USD
https://api.cryptonator.com/api/ticker/btc-usd

RETURNS (EN FORMATO JSON):

{"ticker":{"base":"BTC","target":"USD","price":"443.7807865468","volume":"31720.1493969300","change":"0.3766203596"},"timestamp":1399490941,"success":true,"error":""}

#### OTROS EJEMPLOS:
https://api.cryptonator.com/api/ticker/btc-eur

https://api.cryptonator.com/api/ticker/eur-eth

https://api.cryptonator.com/api/ticker/eth-dai

##### Params

    Base - Base currency code
    Target - Target currency code
    Price - Volume-weighted price
    Volume - Total trade volume for the last 24 hours
    Change - Past hour price change
    Timestamp - Update time in Unix timestamp format
    Success - True or false
    Error - Error description

Replace btc-usd with the currency codes you need. Please refer to the actual list of supported currencies. Volume is displayed only for the cryptocurrencies that are actually traded on online exchanges.

##### Complete ticker

Returns actual volume-weighted price, total 24h volume, rate change as well as prices and volumes across all connected exchanges.
Example request for BTC-USD

https://api.cryptonator.com/api/full/btc-usd

##### returns

{"ticker":{"base":"BTC","target":"USD","price":"443.7807865468","volume":"31720.1493969300","change":"0.3766203596","markets":[{"market":"bitfinex","price":"447.5000000000","volume":"10559.5293639000"},{"market":"bitstamp","price":"448.5400000000","volume":"11628.2880079300"},{"market":"btce","price":"432.8900000000","volume":"8561.0563600000"},{"market":"cryptotrade","price":"436.9999989900","volume":"0.3640623100"},{"market":"exmoney","price":"428.0000000000","volume":"7.9020328400"},{"market":"hitbtc","price":"442.6200000000","volume":"750.5900000000"},{"market":"justcoin","price":"453.4920000000","volume":"10.2583700000"},{"market":"kraken","price":"452.7042200000","volume":"17.7767076800"},{"market":"therocktrading","price":"440.0000000000","volume":"178.9300000000"},{"market":"vaultofsatoshi","price":"450.6428600000","volume":"5.3209840100"},{"market":"vircurex","price":"460.0000000000","volume":"0.1335082600"}]},"timestamp":1399490941,"success":true,"error":""}

##### Params

    Base - Base currency code
    Target - Target currency code
    Price - Volume-weighted price
    Volume - Total trade volume for the last 24 hours
    Change - Past hour price change
    Markets - Array with prices/volumes across all exchanges
        Market - Name of the exchange
        Price - Price on this exchange
        Volume - 24h trade volume on this exchange
    Timestamp - Update time in Unix timestamp format
    Success - True or false
    Error - Error description

Replace btc-usd with the currency codes you need. Please refer to the actual list of supported currencies. Volume is displayed only for the cryptocurrencies that are actually traded on online exchanges.

##### Conversion ticker
In development, coming soon. Stay tuned @cryptonatorcom
-----------------------------------------------------------------------------

### COINSWITCH: Crypto Trading Simplified - Trade 300+ coins without creating an account on any exchange.
https://coinswitch.co/

https://developer.coinswitch.co/

https://developer.coinswitch.co/reference
-----------------------------------------------------------------------------

### BINANCE:

https://api.binance.com

https://binance-docs.github.io/apidocs/spot/en/#current-average-price
##### ESTO FUNCIONA:

https://api.binance.com/api/v1/ticker/24hr
##### GENERA UN JSON CON 7 PARES DE VALORES
##### E INCLUYE TODOS LOS PARES ¡¡¡800 EN TOTAL!!!

https://api.binance.com/api/v1/ticker/24hr/ETHBTC
##### EL 0 ES ETHBTC: (COPIO EL CONTENIDO JSON)
##### ETHBTC

[{"symbol":"ETHBTC","priceChange":"-0.00083200","priceChangePercent":"-3.628","weightedAvgPrice":"0.02269712","prevClosePrice":"0.02293700","lastPrice":"0.02210000","lastQty":"0.00600000","bidPrice":"0.02209600","bidQty":"0.00900000","askPrice":"0.02210000","askQty":"0.00300000","openPrice":"0.02293200","highPrice":"0.02309600","lowPrice":"0.02205000","volume":"212161.37200000","quoteVolume":"4815.45145462","openTime":1588709272846,"closeTime":1588795672846,"firstId":174468126,"lastId":174571623,"count":103498},{"symbol":"LTCBTC","...

##### ETHEUR
{"symbol":"ETHEUR","priceChange":"-0.44000000","priceChangePercent":"-0.232","weightedAvgPrice":"192.61895277","prevClosePrice":"189.79000000","lastPrice":"189.56000000","lastQty":"0.72276000","bidPrice":"189.56000000","bidQty":"0.80731000","askPrice":"190.44000000","askQty":"0.46540000","openPrice":"190.00000000","highPrice":"195.39000000","lowPrice":"187.83000000","volume":"515.65817000","quoteVolume":"99325.53669380","openTime":1588709167932,"closeTime":1588795567932,"firstId":101893,"lastId":102771,"count":879},...

-----------------------------------------------------------------------------

### COIN MARKET CAP:
https://coinmarketcap.com/

https://coinmarketcap.com/api/documentation/v1/

##### Market Pairs Latest:
A PARTIR DEL PLAN STANDARD:
Standard-Suitable for most use cases: $299/ month, pago anual!!!

https://coinmarketcap.com/api/documentation/v1/#operation/getV1CryptocurrencyMarketpairsLatest

-----------------------------------------------------------------------------

### ESTE LO SACA EN LA WEB, PERO HABRÍA QUE VER SI SE PUEDEN SACAR FACILMENTE LOS VALORES:
https://bitinfocharts.com/cryptocurrency-prices/

-----------------------------------------------------------------------------

### PENDIENTE DE HACER PRUEBAS CON ESTE SITIO
https://www.cryptoratesxe.com/
##### PARA INTEGRARLO EN UNA WEB:
<a id="CA_index.html" title="Cryptocurrency exchange rates" target="_blank" href="https://www.cryptoratesxe.com/index.html">Cryptocurrency exchange rates</a><script>(function(c){document.write('<span id="'+c+'"></span>');r=false;s=document.createElement('script');s.async=1;s.src='https://www.cryptoratesxe.com/js/outbox.js?t=2&calc='+c;s.onload =s.onreadystatechange=function(){(!r&&(!this.readyState||this.readyState=='complete')&&(r=true))};t=document.getElementsByTagName('script')[0];t.parentNode.insertBefore(s,t);})('index.html');</script>

-----------------------------------------------------------------------------

#### EJEMPLO BINANCE, DE JOSEP, PARA SACAR EL PRECIO:
LO MISMO SE PUEDE TRATAR DE ADAPTAR PARA SACAR EL PRECIO DESDE:
CRYPTONATOR
##### binance-orig.js
https://github.com/servatj/binance-bot/blob/master/src/binance.js
------------------------------------------------------------------------
const axios = require('axios');

const BINANCE = 'https://api.binance.com';
const URL_TICKER = 'api/v1/ticker/24hr';


const createRequest = ({ baseUrl, endpoint }) => `${baseUrl}/${endpoint}`;
const runRequest = (request) => axios.get(request);

const getAssetInfo = (coin) => {
  return runRequest(createRequest( { baseUrl: BINANCE, query: URL_TICKER + '/' + coin } ))
}

module.exports = {
  createRequest,
  getAssetInfo
}
------------------------------------------------------------------------






