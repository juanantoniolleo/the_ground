## Oráculos
Entiendo como Oráculo, en este caso, el mecanismo que permite saber el precio de las criptomonedas de referencia, con la suficiente fiabilidad.

##### NOTAS
Hay que definir exactamente las funciones que se necesitan del Oráculo:
- Comprobar el valor de pares: ETH-USD, por ejemplo
- Seleccionar un sitio donde conseguir el valor
- Lo mismo pueden ser varios sitios y calcular una media, por ejemplo
- Hay que establecer un intervalo de actualización de los valores
- También hay que definir cómo integrar esas consultas en los contratos inteligentes del proyecto
- A veces es difícil localizar exactamente la función que se encarga de esa función en los proyectos
- Por ejemplo, Uniswap, Balancer


#### PENDIENTE:
##### Probar:
- En este ejemplo, tratar de conseguir que lea referencias a criptomonedas:

oracleEd01.sol

- Analizar el contrato, probarlo en una testnet. Repasar todos los contratos que llama.

ExampleOracleSimple-Comentado01.sol 