### Texto para mandar a la convocatoria de Codemotion:

### TITULO:
### "THE WALL: iniciación al desarrollo DeFi con Ethereum y Balancer"
Workshop, intermedio, español, de 90' a 180'
Juan Antonio Lleó y Jesús Pérez
Crypto Plaza DEVS

### Descripción:
En este taller se va a aprender a trabajar con el conjunto de contratos inteligentes que forman parte del protocolo de DeFi Balancer. En una primera parte se va a analizar las herramientas necesarias para poder crear, editar y desplegar Smart Contracts sobre Solidity, usando la herramienta en linea Remix y la solución para desarrollo en local, compuesta por Truffle y Ganache y cómo poder juntarlas también con Remix.

El código Solidity del protocolo Balancer está disponible en su repositorio de GitHub, con lo que se puede clonar y basarse en el mismo para añadir otros contratos que aporten funcionalidades distintas de las previstas inicialmente. Pueden hacerse pruebas en distintas blockchain de test, que podrán desplegarse tanto en remoto como en local y también se podría desplegar, si tuviera sentido, sobre la red principal de Ethereum.

Como ejemplo, se va a tratar el desarrollo de The Wall, proyecto que se presentó al pasado Hackcthon Hack The Money y que consiste en un contrato inteligente que, mediante las sucesivas consultas a oráculos de precios, es capaz de determinar si ciertos cambios bruscos en la cotización de Ethereum son debidos a movimientos de cuentas con mucho capital, conocidos como whales (ballenas), con el objetivo de contrarrestar sus efectos y conseguir beneficios, ajustando el precio de las comisiones de un pool.